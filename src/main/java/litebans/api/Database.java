package litebans.api;

import litebans.api.exception.MissingImplementationException;
import lombok.Setter;
import org.jetbrains.annotations.*;

import java.sql.*;
import java.util.UUID;

/**
 * Created by ruan on 2017-03-23 at 09:22.
 * <p>
 * This class allows for database queries to be executed on players.
 * Queries are completely thread-safe and can be executed concurrently.
 * Performing too many concurrent queries may exhaust the connection pool (default max 10 connections).
 * Under Bukkit, queries will throw an IllegalStateException if executed on the main server thread.
 */
public abstract class Database {
    @Setter
    private static Database instance;

    public static Database get() {
        if (instance == null) throw new MissingImplementationException();
        return instance;
    }

    /**
     * @param uuid UUID of player, can be null
     * @param ip   IP address of player, can be null
     * @return true if there is an active ban matching `uuid` or `ip`, false otherwise
     */
    public abstract boolean isPlayerBanned(@Nullable UUID uuid, @Nullable String ip);

    /**
     * @param uuid UUID of player, can be null
     * @param ip   IP address of player, can be null
     * @return true if there is an active mute matching `uuid` or `ip`, false otherwise
     */
    public abstract boolean isPlayerMuted(@Nullable UUID uuid, @Nullable String ip);

    /**
     * Example:
     * <p>
     * try (PreparedStatement st = Database.get().prepareStatement("SELECT * FROM {bans}")) {
     * <p>
     * try (ResultSet rs = st.executeQuery()) {
     * <p>
     * ...
     * <p>
     * }
     * <p>
     * }
     *
     * @param sql SQL query. Tokens {bans}, {mutes}, {warnings}, {kicks}, {history}, {servers} will be replaced with real table names including the configured table prefix.
     */
    public abstract PreparedStatement prepareStatement(@NotNull String sql) throws SQLException;
}
